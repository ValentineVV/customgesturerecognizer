//
//  CustomGestureRecognizer.h
//  UserForm
//
//  Created by Valiantsin Vasiliavitski on 4/5/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum CheckPhases {
    notStarted,
    initialPoint,
    movingStroke
} CheckPhases;

@interface CustomGestureRecognizer : UITapGestureRecognizer

@property (nonatomic, strong) UITouch *firstTouch;
@property (nonatomic, strong) UITouch *secondTouch;
@property (nonatomic) CGPoint initialTouchPoint1;
@property (nonatomic) CGPoint initialTouchPoint2;
@property (nonatomic) CheckPhases strokePhase;

@end
