//
//  CustomGestureRecognizer.m
//  UserForm
//
//  Created by Valiantsin Vasiliavitski on 4/5/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "CustomGestureRecognizer.h"
#import "UIKit/UIGestureRecognizerSubclass.h"

@implementation CustomGestureRecognizer

-(instancetype)init {
    if (self = [super init]) {
        self.strokePhase = notStarted;
        self.initialTouchPoint1 = CGPointZero;
        self.initialTouchPoint2 = CGPointZero;
        self.firstTouch = nil;
        self.secondTouch = nil;
//        self.numberOfTouchesRequired = 2;
    }
    
    return self;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    if ([touches count] != 2) {
        self.state = UIGestureRecognizerStateFailed;
        return;
    }
    
    if (self.firstTouch == nil && self.secondTouch == nil) {
        NSArray *arr = [touches allObjects];
        self.firstTouch = arr[0];
        self.secondTouch = arr[1];
        self.strokePhase = initialPoint;
        self.initialTouchPoint1 = [self.firstTouch locationInView:self.view];
        self.initialTouchPoint2 = [self.secondTouch locationInView:self.view];
        if (fabs(self.initialTouchPoint1.y - self.initialTouchPoint2.y) < 100) {
            self.state = UIGestureRecognizerStateFailed;
        }
    } else {
        for (UITouch *touch in touches) {
            if (touch != self.firstTouch && touch != self.secondTouch) {
                [self ignoreTouch:touch forEvent:event];
            }
        }
    }
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    
    [super touchesMoved:touches withEvent:event];
    NSArray *arr = [touches allObjects];
    UITouch *newTouch1 = arr[0];
    UITouch *newTouch2 = arr[1];
    if (newTouch1 != self.firstTouch && newTouch2 != self.secondTouch) {
        if (newTouch1 != self.secondTouch && newTouch2 != self.firstTouch) {
            self.state = UIGestureRecognizerStateFailed;
            return;
        }
                UITouch *tmp = newTouch1;
                newTouch1 = newTouch2;
                newTouch2 = tmp;
    }
    CGPoint newPoint1 = [newTouch1 locationInView:self.view];
    CGPoint newPoint2 =[newTouch2 locationInView:self.view];
    
    CGPoint previousPoint1 = [newTouch1 previousLocationInView:self.view];
    CGPoint previousPoint2 = [newTouch2 previousLocationInView:self.view];
//    BOOL flag = NO;
    if (self.strokePhase == initialPoint) {
        if (newPoint1.y > self.initialTouchPoint1.y && newPoint2.y < self.initialTouchPoint2.y) {
            self.strokePhase = movingStroke;
//            flag = YES;
        } else if (newPoint1.y < self.initialTouchPoint1.y && newPoint2.y > self.initialTouchPoint2.y) {
            self.strokePhase = movingStroke;
//            flag = NO;
        } else {
            self.state = UIGestureRecognizerStateFailed;
        }
    } else if (self.strokePhase == movingStroke) {
//        if (flag) {
            [self movingFirstPoint:newPoint1 previousFirstPoint:previousPoint1 secondPoint:newPoint2 previousSecondPoint:previousPoint2];
//        } else {
//            [self movingFirstPoint:newPoint2 previousFirstPoint:previousPoint2 secondPoint:newPoint1 previousSecondPoint:previousPoint1];
//        }
    }
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [super touchesEnded:touches withEvent:event];
    NSArray *arr = [touches allObjects];
    UITouch *newTouch1 = arr[0];
    UITouch *newTouch2 = arr[1];
    if (newTouch1 != self.firstTouch || newTouch2 != self.secondTouch) {
        if (newTouch1 != self.secondTouch || newTouch2 != self.firstTouch) {
            self.state = UIGestureRecognizerStateFailed;
            return;
        }
        UITouch *tmp = newTouch1;
        newTouch1 = newTouch2;
        newTouch2 = tmp;
    }
    
    CGPoint newPoint1 = [newTouch1 locationInView:self.view];
    CGPoint newPoint2 =[newTouch2 locationInView:self.view];
    
    if (self.state == UIGestureRecognizerStatePossible && self.strokePhase == movingStroke &&
        ((newPoint1.y > self.initialTouchPoint1.y && newPoint1.y >= self.initialTouchPoint2.y && newPoint2.y < self.initialTouchPoint2.y && newPoint2.y <= self.initialTouchPoint1.y) || (newPoint1.y < self.initialTouchPoint1.y && newPoint1.y <= self.initialTouchPoint2.y && newPoint2.y > self.initialTouchPoint2.y && newPoint2.y >= self.initialTouchPoint1.y))) {
        self.state = UIGestureRecognizerStateRecognized;
    } else {
        self.state = UIGestureRecognizerStateFailed;
//        self.state = UIGestureRecognizerStateCancelled;
    }
    
    if (fabs(newPoint1.y - self.initialTouchPoint1.y) < 0.1 || fabs(newPoint2.y - self.initialTouchPoint2.y) < 0.1) {
        self.state = UIGestureRecognizerStateFailed;
    }
//    self.state = UIGestureRecognizerStateFailed;
}

- (void)movingFirstPoint:(CGPoint)newPoint1 previousFirstPoint:(CGPoint)previousPoint1 secondPoint:(CGPoint)newPoint2 previousSecondPoint:(CGPoint)previousPoint2 {
    if (newPoint1.y > previousPoint1.y && newPoint2.y < previousPoint2.y) {
        if (fabs(newPoint1.x - self.initialTouchPoint1.x) > 30.0 || fabs(newPoint2.x - self.initialTouchPoint2.x) > 30.0) {
            self.state = UIGestureRecognizerStateFailed;
        }
    } else if (newPoint1.y < previousPoint1.y && newPoint2.y > previousPoint2.y) {
        if (fabs(newPoint1.x - self.initialTouchPoint1.x) > 30.0 || fabs(newPoint2.x - self.initialTouchPoint2.x) > 30.0) {
            self.state = UIGestureRecognizerStateFailed;
        }
    } else {
        self.state = UIGestureRecognizerStateFailed;
    }
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    self.initialTouchPoint1 = CGPointZero;
    self.initialTouchPoint2 = CGPointZero;
    self.strokePhase = notStarted;
    self.firstTouch = nil;
    self.secondTouch = nil;
    self.state = UIGestureRecognizerStateCancelled;
}

- (void)reset {
    
    [super reset];
    self.initialTouchPoint1 = CGPointZero;
    self.initialTouchPoint2 = CGPointZero;
    self.strokePhase = notStarted;
    self.firstTouch = nil;
    self.secondTouch = nil;
}



@end
