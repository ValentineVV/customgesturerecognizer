//
//  ViewController.m
//  GestureRecognizer
//
//  Created by Valiantsin Vasiliavitski on 4/9/18.
//  Copyright © 2018 Valiantsin Vasiliavitski. All rights reserved.
//

#import "ViewController.h"
#import "CustomGestureRecognizer.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    CustomGestureRecognizer *recognizer = [[CustomGestureRecognizer alloc] initWithTarget:self action:@selector(customRecognizerRecognized)];
    recognizer.numberOfTouchesRequired = 2;
    [self.view addGestureRecognizer:recognizer];
}

- (void)customRecognizerRecognized {
    NSLog(@"CustomGestureRecognizer was recognized");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


@end
